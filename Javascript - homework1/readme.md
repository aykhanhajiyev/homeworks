1st question:<br/>
<b>var</b> variables can be updated and re-declared within its scope.
<b>let</b> variables can be updated but not re-declared 
<b>const</b> variables can neither be updated nor re-declared. 
They are all hoisted to the top of their scope but while var variables 
are initialized with undefined,let and const variables are not initialized.

2st question:<br/>
Variables declared with var are not block scoped 
although they are function scoped, while with let and const 
they are. This is important because what's the point of block 
scoping if you're not going to use it.

