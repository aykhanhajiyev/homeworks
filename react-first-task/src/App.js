import React, {Component} from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    state = {
        isShowButtonContainer: true,
        isShowModal: false,
        isShowModalSecond:false
    };
    OnClickHandlerFirstModal = () => {
            this.setState({
                isShowButtonContainer: !this.state.isShowButtonContainer,
                isShowModal: !this.state.isShowModal
            });
    }
    OnClickOutsideContentHandler = (e)=>{
        if(e.currentTarget === e.target){
            this.setState({
                isShowButtonContainer: !this.state.isShowButtonContainer,
                isShowModal: !this.state.isShowModal
            });
        }
    }
    OnClickHandlerSecondModal = () => {
        this.setState({
            isShowButtonContainer: !this.state.isShowButtonContainer,
            isShowModalSecond: !this.state.isShowModalSecond
        });
    }
    OnClickOutsideContentHandlerSecond = (e)=>{
        if(e.currentTarget === e.target){
            this.setState({
                isShowButtonContainer: !this.state.isShowButtonContainer,
                isShowModalSecond: !this.state.isShowModalSecond
            });
        }
    }
    render() {
        return (
            <div className="App">
                <div className={'buttons'}
                     style={(this.state.isShowButtonContainer) ? {display: "block"} : {display: "none"}}>
                    <Button backgroundColor={"red"} text={"Open first modal"}
                            clickHandler={this.OnClickHandlerFirstModal}/>
                    <Button backgroundColor={"teal"} text={"Open second modal"}
                            clickHandler={this.OnClickHandlerSecondModal}/>
                </div>
                <Modal isShow={this.state.isShowModal} header={"Do you want to delete this file?"}
                       closeButton={this.OnClickHandlerFirstModal}
                       text={"Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"}
                       closeoutside={this.OnClickOutsideContentHandler}
                />
                <Modal isShow={this.state.isShowModalSecond} header={"It is a bit complicated staff"}
                       closeButton={this.OnClickHandlerSecondModal}
                       text={"Today is 8th of March! Happy Women's Day! Idk why I wrote it"}
                       closeoutside={this.OnClickOutsideContentHandlerSecond}
                />
            </div>
        );
    }
}

export default App;
