import React from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';
const Modal = (props) => {
    return (
        <div className={'modal-bg'} style={(props.isShow) ? {display:"block"} : {display:"none"}} onClick={props.closeoutside}>
            <div className={'modal'}>
                <div className={'modal-header'}>
                    <div className={'modal-header-title'}>{props.header}</div>
                    <div className={'modal-header-close'} onClick={props.closeButton}>
                        <div className={'modal-close-line'}></div>
                        <div className={'modal-close-line-opposite'}></div>
                    </div>
                </div>
                <div className={'modal-container'}>
                    <div className={'modal-container-text'}>
                        <p className={'modal-container-text-main'}>{props.text}</p>
                    </div>
                    <div className={'modal-container-buttons'}>
                        <button type={'button'} className={'modal-button'} >Ok</button>
                        <button type={'button'} className={'modal-button'}>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    );
};
Modal.propTypes ={
    isShow:PropTypes.bool,
    closeoutside:PropTypes.func,
    header:PropTypes.string
};
export default Modal;