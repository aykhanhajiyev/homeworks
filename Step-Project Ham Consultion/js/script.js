const tabs = document.querySelectorAll(".service-tab");
const details_tabs = document.querySelectorAll('.service-detail');
//Initial active first tab and service-detail show
tabs[0].classList.add('service-active');
details_tabs[0].classList.remove('d-none');
for (let tab of tabs) {
    tab.addEventListener('click', function () {
        deactivatealltabs(tabs);
        this.classList.add('service-active');
        showservicedetail(Array.from(tabs).indexOf(tab));
    })
}

function deactivatealltabs(tabs) {
    for (let tab of tabs) {
        tab.classList.remove("service-active");
    }
}

function showservicedetail(id) {
    for (let detail of details_tabs) {
        detail.classList.add('d-none');
    }
    details_tabs[id].classList.remove('d-none');
}

//Our Amazing work
const works_categories = document.querySelectorAll('.work-category-item');
for (let work of works_categories) {
    work.addEventListener('click', function () {
        deactiveallcategories(works_categories);
        this.classList.add('category-active');
    })
}

function deactiveallcategories(categories) {
    for (let category of categories) {
        category.classList.remove('category-active');
    }
}

//Load Images with Load More button
let counter = 0;
const button = document.querySelector('.btn-load-more');
const loader = document.querySelector('.loader');
const gallery_items = document.querySelectorAll('.gallery-item');
for (let i = 0; i < gallery_items.length; i++) {
    if (i < 12) {
        gallery_items[i].classList.remove('gallery-item-d-none');
    } else {
        gallery_items[i].classList.add('gallery-item-d-none');
    }
}
//Hover up
for(let item of gallery_items){
    item.addEventListener('mouseenter',function(){
        const gallery_item_hover = document.createElement('div');
        const gallery_item_hover_content_icons = document.createElement('div');
        const gallery_item_hover_icons = document.createElement('div');
        const gallery_item_hover_icon = document.createElement('div');
        const gallery_item_hover_icon_search = document.createElement('div');
        const fas_fa_link = document.createElement("i");
        const fas_fa_search = document.createElement("i");
        const gallery_item_hover_content = document.createElement('div');
        const gallery_item_hover_header = document.createElement('div');
        const gallery_item_hover_text = document.createElement('div');

        gallery_item_hover.classList.add('gallery-item-hover');
        gallery_item_hover_content_icons.classList.add('gallery-item-hover-content-icons');
        gallery_item_hover_icon.classList.add('gallery-item-hover-icon');
        gallery_item_hover_icon_search.classList.add('gallery-item-hover-icon');
        fas_fa_link.classList.add('fas');
        fas_fa_link.classList.add('fa-link');
        fas_fa_search.classList.add('fas');
        fas_fa_search.classList.add('fa-search');
        gallery_item_hover_content.classList.add('gallery-item-hover-content');
        gallery_item_hover_header.classList.add('gallery-item-hover-header');
        gallery_item_hover_text.classList.add('gallery-item-hover-text');

        gallery_item_hover_icon.append(fas_fa_link);
        gallery_item_hover_icon_search.append(fas_fa_search);
        gallery_item_hover_content_icons.append(gallery_item_hover_icon,gallery_item_hover_icon_search);
        gallery_item_hover_header.textContent="Creative Design";
        gallery_item_hover_text.textContent=this.dataset.type;
        gallery_item_hover_content.append(gallery_item_hover_header,gallery_item_hover_text);

        gallery_item_hover.append(gallery_item_hover_content_icons,gallery_item_hover_content);

        this.children[0].after(gallery_item_hover);

    });
    item.addEventListener('mouseleave',function(){
        item.querySelector('.gallery-item-hover').remove();
    })
}

button.addEventListener('click', function () {
    if (counter <= 2) {
        counter++;
        showloader();
        setTimeout(() => {
            for (let i = 0; i < gallery_items.length; i++) {
                if (counter === 1) {
                    if (i >= 12 && i < 24) {
                        gallery_items[i].classList.remove('gallery-item-d-none')
                    }
                }
                if(counter===2){
                    if(i>=24){
                        gallery_items[i].classList.remove('gallery-item-d-none');
                    }
                }
            };
            loader.classList.add('d-none');
        }, 2000);
    }
    if (counter === 2) {
        button.classList.add('d-none');
    }
});

function showloader() {
    loader.classList.remove('d-none');
}

//Filter by Category
const categories = document.querySelectorAll('.work-category-item');

for (let category of categories) {
    category.addEventListener('click', function () {
        for (let item of gallery_items) {
            item.classList.remove("d-none");
        }
        for (let item of gallery_items) {
            if (this.dataset.type === "all") {
                item.classList.remove('d-none');
                break;
            }
            if (this.dataset.type !== item.dataset.type) {
                item.classList.add('d-none');
            }
        }
    });
}