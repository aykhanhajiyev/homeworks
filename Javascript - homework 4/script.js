function filterBy(arr,dataType) {
    let newArr = [];
    for (let item of arr) {
        if(typeof item !==dataType){
            newArr.push(item);
        }
    }
    return newArr;
}

console.log(filterBy(['hello', 'world', '23', 23, null], "string"));;