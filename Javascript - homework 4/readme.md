forEach executes the callback function once for each array element.
It always returns undefined. It does not mutate the array, but
the callback can if programmed to do so. forEach is not chain-able like map, reduce or filter.