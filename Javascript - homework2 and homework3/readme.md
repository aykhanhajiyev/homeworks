1st question:<br/>
Functions are "self contained" modules of code that accomplish a 
specific task. Functions usually "take in" data, process it, and 
"return" a result. Once a function is written, it can be used over 
and over and over again. Functions can be "called" from the inside of other 
functions.

2nd question: <br/>
Except for functions with variable-length argument lists, 
the number of arguments in a function call must be the same 
as the number of parameters in the function definition. 
This number can be zero.
Arguments are separated by commas. However, the comma is not 
an operator in this context, and the arguments can be evaluated by the compiler in any order. 
There is, however, a sequence point before the actual call.

3nd question: <br/>
Methods are actions that can be performed on objects. 
Object properties can be both primitive values, 
other objects, and functions. An object method 
is an object property containing a function
definition. JavaScript objects are containers 
for named values, called properties and methods