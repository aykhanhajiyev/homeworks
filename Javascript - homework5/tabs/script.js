const tabs_function = document.getElementsByClassName("tabs-title");
const tabs_content = document.querySelector(".tabs-content");
//Adding tabs_content all child id start 1 to it length
let id_tabs = 0;
for (let tab of tabs_content.children) {
    tab.id = (++id_tabs).toString();
}
id_tabs = 0;
for (let func of tabs_function) {
    //Add id tabs_function
    func.id = "#" + (++id_tabs);
    func.addEventListener("click", function () {
        //First remove all active class all elements
        for (let item of tabs_function)
            item.classList.remove("active");
        //Then here implement active class which clicked

        func.classList.add("active");
        if (func.classList.contains("active")) {
            const id = func.id.replace("#", ""); //get Id without #
            for (let tab of tabs_content.children) {
                if (tab.id === id) tab.classList.remove('d-none');
                else tab.classList.add('d-none');
            }
        }
    });
}
